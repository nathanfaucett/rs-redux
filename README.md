redux
=====

a redux in rust

```rust
extern crate redux;


use redux::*;


#[derive(Debug)]
enum CounterAction {
    Inc,
    Dec,
}

impl Action for CounterAction {
    type State = Counter;
}

#[derive(Debug, PartialEq)]
pub struct Counter {
    count: isize
}

impl Counter {
    pub fn new(count: isize) -> Self {
        Counter {
            count: count,
        }
    }
    pub fn inc(&self) -> Self {
        Self::new(self.count + 1)
    }
    pub fn dec(&self) -> Self {
        Self::new(self.count - 1)
    }
}

fn counter(action: CounterAction, state: &Counter) -> Counter {
    match action {
        CounterAction::Inc => state.inc(),
        CounterAction::Dec => state.dec(),
    }
}


#[derive(Debug)]
enum TotalAction {
    Count,
}

impl Action for TotalAction {
    type State = Total;
}

#[derive(Debug, PartialEq)]
pub struct Total {
    count: usize
}

impl Total {
    pub fn new(count: usize) -> Self {
        Total {
            count: count,
        }
    }
    pub fn inc(&self) -> Self {
        Self::new(self.count + 1)
    }
}

fn count(action: TotalAction, state: &Total) -> Total {
    match action {
        TotalAction::Count => state.inc(),
    }
}


fn count_middleware(action: &CounterAction, store: &Store) {
    match action {
        &CounterAction::Inc => store.dispatch(TotalAction::Count),
        &CounterAction::Dec => store.dispatch(TotalAction::Count),
    };
}



fn main() {
    let store = StoreBuilder::new()
        .reducer(Counter::new(0), counter)
        .reducer(Total::new(0), count)
        .middleware(count_middleware)
        .build();

    let initial_state = store.get_state();

    store.dispatch(CounterAction::Inc);
    assert_eq!(store.get_state().get::<Counter>().unwrap(), &Counter::new(1));

    store.dispatch(CounterAction::Dec);
    store.dispatch(CounterAction::Dec);
    assert_eq!(store.get_state().get::<Counter>().unwrap(), &Counter::new(-1));

    store.run(|s| {
        for _ in 0..100 {
            s.dispatch(CounterAction::Inc);
        }
    });

    assert_eq!(store.get_state().get::<Counter>().unwrap(), &Counter::new(99));
    assert_eq!(store.get_state().get::<Total>().unwrap(), &Total::new(103));

    assert_eq!(initial_state.get::<Counter>().unwrap(), &Counter::new(0));
}
```
