

pub trait Action: 'static {
    type State: 'static;
}
