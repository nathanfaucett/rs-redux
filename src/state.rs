use std::any::{Any, TypeId};
use std::collections::HashMap;
use std::sync::Arc;


#[derive(Clone)]
pub struct State {
    current: HashMap<TypeId, Arc<Any>>,
}

unsafe impl Sync for State {}
unsafe impl Send for State {}

impl State {
    #[inline(always)]
    pub fn new() -> Self {
        State {
            current: HashMap::new(),
        }
    }

    #[inline]
    pub fn set<T: 'static>(&self, state: T) -> Self {
        let mut next = self.current.clone();

        next.insert(TypeId::of::<T>(), Arc::new(state));

        State { current: next }
    }

    #[inline(always)]
    pub(crate) fn set_mut<T: 'static>(&mut self, state: T) -> &mut Self {
        self.current.insert(TypeId::of::<T>(), Arc::new(state));
        self
    }

    #[inline]
    pub fn get<T: 'static>(&self) -> Option<&T> {
        match self.current.get(&TypeId::of::<T>()) {
            Some(state) => state.downcast_ref::<T>(),
            None => None,
        }
    }
}


#[cfg(test)]
mod test {
    use super::*;


    #[test]
    fn test() {
        let mut state = State::new();

        state.set_mut::<usize>(1);
        state.set_mut::<isize>(-1);

        assert_eq!(state.get::<usize>().unwrap(), &1);
        assert_eq!(state.get::<isize>().unwrap(), &-1);

        let new_usize_state = state.set::<usize>(2);
        let new_isize_state = new_usize_state.set::<isize>(-2);

        assert_eq!(new_usize_state.get::<usize>().unwrap(), &2);
        assert_eq!(new_isize_state.get::<isize>().unwrap(), &-2);

        assert_eq!(state.get::<usize>().unwrap(), &1);
        assert_eq!(state.get::<isize>().unwrap(), &-1);
    }
}
