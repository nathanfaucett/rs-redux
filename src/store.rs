use std::any::{Any, TypeId};
use std::collections::HashMap;
use std::sync::Mutex;
use std::convert::From;
use std::marker::PhantomData;

use super::action::Action;
use super::state::State;
use super::store_builder::StoreBuilder;


pub struct StoreGuard<'a, A>
where
    A: Action,
{
    store: &'a Store,
    marker: PhantomData<&'a A>,
}

impl<'a, A> StoreGuard<'a, A>
where
    A: Action,
{
    #[inline(always)]
    pub fn new(store: &'a Store) -> Self {
        StoreGuard {
            store: store,
            marker: PhantomData,
        }
    }

    #[inline(always)]
    pub fn dispatch(&self, action: A)
    where
        A: Action,
    {
        self.store.dispatch_middleware(&action);
        self.store.dispatch_nolock(action);
    }
}


pub struct Store {
    state: *mut State,
    state_lock: Mutex<()>,
    functions: HashMap<TypeId, Box<Any>>,
    middlewares: HashMap<TypeId, Vec<Box<Any>>>,
    dispatch_locks: HashMap<TypeId, Mutex<()>>,
}

unsafe impl Send for Store {}
unsafe impl Sync for Store {}

impl From<StoreBuilder> for Store {
    #[inline]
    fn from(store_builder: StoreBuilder) -> Self {
        let size = store_builder.functions.len();
        let mut dispatch_locks = HashMap::with_capacity(size);

        for (type_id, _) in store_builder.functions.iter() {
            dispatch_locks.insert(type_id.clone(), Mutex::new(()));
        }

        Store {
            state: Box::into_raw(Box::new(store_builder.state)),
            state_lock: Mutex::new(()),
            functions: store_builder.functions,
            middlewares: store_builder.middlewares,
            dispatch_locks: dispatch_locks,
        }
    }
}

impl Store {
    #[inline(always)]
    fn state_as_ptr(&self) -> *mut State {
        self.state
    }
    #[inline(always)]
    fn state_as_ref(&self) -> &State {
        unsafe { &*self.state_as_ptr() }
    }
    #[inline(always)]
    fn state_as_mut(&self) -> &mut State {
        unsafe { &mut *self.state_as_ptr() }
    }

    #[inline]
    fn dispatch_nolock<A>(&self, action: A) -> &Self
    where
        A: Action,
    {
        let type_id = TypeId::of::<A::State>();

        let function = self.functions
            .get(&type_id)
            .unwrap()
            .downcast_ref::<fn(A, &A::State) -> A::State>()
            .unwrap();

        let new_state = {
            let old_state = self.state_as_ref().get::<A::State>().unwrap();
            (function)(action, old_state)
        };

        let _guard = match self.state_lock.lock() {
            Ok(guard) => guard,
            Err(poison) => poison.into_inner(),
        };
        self.state_as_mut().set_mut(new_state);
        self
    }

    #[inline]
    fn dispatch_middleware<A>(&self, action: &A)
    where
        A: Action,
    {
        let type_id = TypeId::of::<A::State>();

        match self.middlewares.get(&type_id) {
            Some(middlewares) => {
                for middleware in middlewares.iter() {
                    let function = middleware.downcast_ref::<fn(&A, &Self)>().unwrap();
                    (function)(action, self);
                }
            }
            None => (),
        }
    }


    #[inline(always)]
    pub fn get_state(&self) -> State {
        self.state_as_ref().clone()
    }

    #[inline]
    pub fn dispatch<A>(&self, action: A) -> &Self
    where
        A: Action,
    {
        self.dispatch_middleware(&action);

        let _guard = match self.dispatch_locks
            .get(&TypeId::of::<A::State>())
            .unwrap()
            .lock() {
            Ok(guard) => guard,
            Err(poison) => poison.into_inner(),
        };

        self.dispatch_nolock(action)
    }

    #[inline]
    pub fn run<'a, A, F>(&'a self, f: F)
    where
        A: Action,
        F: Fn(StoreGuard<'a, A>),
    {
        let _guard = match self.dispatch_locks
            .get(&TypeId::of::<A::State>())
            .unwrap()
            .lock() {
            Ok(guard) => guard,
            Err(poison) => poison.into_inner(),
        };

        f(StoreGuard::new(self));
    }
}


#[cfg(test)]
mod test {
    use std::sync::Arc;
    use std::thread;

    use super::*;


    macro_rules! counter {
        ($Action: ident, $State: ident, $Function: ident) => (
            #[derive(Debug)]
            enum $Action {
                Inc,
                Dec,
            }

            impl Action for $Action {
                type State = $State;
            }

            #[derive(Debug, PartialEq)]
            pub struct $State {
                count: isize
            }

            impl $State {
                pub fn new(count: isize) -> Self {
                    $State {
                        count: count,
                    }
                }
                pub fn inc(&self) -> Self {
                    Self::new(self.count + 1)
                }
                pub fn dec(&self) -> Self {
                    Self::new(self.count - 1)
                }
            }

            fn $Function(action: $Action, state: &$State) -> $State {
                match action {
                    $Action::Inc => state.inc(),
                    $Action::Dec => state.dec(),
                }
            }
        );
    }

    counter!(CounterAction0, Counter0, counter_reducer0);
    counter!(CounterAction1, Counter1, counter_reducer1);
    counter!(CounterAction2, Counter2, counter_reducer2);
    counter!(CounterAction3, Counter3, counter_reducer3);
    counter!(CounterAction4, Counter4, counter_reducer4);
    counter!(CounterAction5, Counter5, counter_reducer5);
    counter!(CounterAction6, Counter6, counter_reducer6);
    counter!(CounterAction7, Counter7, counter_reducer7);
    counter!(CounterAction8, Counter8, counter_reducer8);
    counter!(CounterAction9, Counter9, counter_reducer9);


    macro_rules! create_handle {
        ($handlers: ident, $store: ident, $Action: ident, $State: ident) => (
            {
                let store = $store.clone();

                $handlers.push(thread::spawn(move || {

                    let initial_state = store.get_state();

                    store.dispatch($Action::Inc);
                    assert_eq!(store.get_state().get::<$State>().unwrap(), &$State::new(1));

                    store.dispatch($Action::Dec);
                    store.dispatch($Action::Dec);
                    assert_eq!(store.get_state().get::<$State>().unwrap(), &$State::new(-1));

                    store.dispatch($Action::Inc);
                    store.dispatch($Action::Inc);
                    assert_eq!(store.get_state().get::<$State>().unwrap(), &$State::new(1));

                    store.run(|s| {
                        for _ in 0..1000 {
                            s.dispatch($Action::Inc);
                        }
                        for _ in 0..1000 {
                            s.dispatch($Action::Dec);
                        }
                    });

                    assert_eq!(store.get_state().get::<$State>().unwrap(), &$State::new(1));
                    assert_eq!(initial_state.get::<$State>().unwrap(), &$State::new(0));
                }));
            }
        );
    }


    #[test]
    fn test_state_sync() {
        let store = StoreBuilder::new()
            .reducer::<CounterAction0>(Counter0::new(0), counter_reducer0)
            .reducer::<CounterAction1>(Counter1::new(0), counter_reducer1)
            .reducer::<CounterAction2>(Counter2::new(0), counter_reducer2)
            .reducer::<CounterAction3>(Counter3::new(0), counter_reducer3)
            .reducer::<CounterAction4>(Counter4::new(0), counter_reducer4)
            .reducer::<CounterAction5>(Counter5::new(0), counter_reducer5)
            .reducer::<CounterAction6>(Counter6::new(0), counter_reducer6)
            .reducer::<CounterAction7>(Counter7::new(0), counter_reducer7)
            .reducer::<CounterAction8>(Counter8::new(0), counter_reducer8)
            .reducer::<CounterAction9>(Counter9::new(0), counter_reducer9)
            .build();

        let arc_store = Arc::new(store);
        let mut handlers = Vec::new();

        create_handle!(handlers, arc_store, CounterAction0, Counter0);
        create_handle!(handlers, arc_store, CounterAction1, Counter1);
        create_handle!(handlers, arc_store, CounterAction2, Counter2);
        create_handle!(handlers, arc_store, CounterAction3, Counter3);
        create_handle!(handlers, arc_store, CounterAction4, Counter4);
        create_handle!(handlers, arc_store, CounterAction5, Counter5);
        create_handle!(handlers, arc_store, CounterAction6, Counter6);
        create_handle!(handlers, arc_store, CounterAction7, Counter7);
        create_handle!(handlers, arc_store, CounterAction8, Counter8);
        create_handle!(handlers, arc_store, CounterAction9, Counter9);

        for handle in handlers {
            let _ = handle.join().unwrap();
        }
    }


    counter!(CounterActionA, CounterA, counter_reducer_a);
    counter!(CounterActionB, CounterB, counter_reducer_b);

    fn counter_a_middleware(action: &CounterActionA, store: &Store) {
        match action {
            &CounterActionA::Inc => store.dispatch(CounterActionB::Inc),
            &CounterActionA::Dec => store.dispatch(CounterActionB::Dec),
        };
    }


    #[test]
    fn test_middleware() {
        let store = StoreBuilder::new()
            .reducer::<CounterActionA>(CounterA::new(0), counter_reducer_a)
            .reducer::<CounterActionB>(CounterB::new(0), counter_reducer_b)
            .middleware::<CounterActionA>(counter_a_middleware)
            .build();

        store.dispatch(CounterActionA::Inc);
        assert_eq!(
            store.get_state().get::<CounterA>().unwrap(),
            &CounterA::new(1)
        );
        assert_eq!(
            store.get_state().get::<CounterB>().unwrap(),
            &CounterB::new(1)
        );

        store.dispatch(CounterActionA::Dec);
        store.dispatch(CounterActionA::Dec);
        assert_eq!(
            store.get_state().get::<CounterA>().unwrap(),
            &CounterA::new(-1)
        );
        assert_eq!(
            store.get_state().get::<CounterB>().unwrap(),
            &CounterB::new(-1)
        );
    }


    enum SimpleAction {
        Update(Box<Fn(&Simple) -> Simple>),
    }

    impl Action for SimpleAction {
        type State = Simple;
    }

    #[derive(Debug, PartialEq)]
    pub struct Simple {
        value: isize,
    }

    impl Simple {
        pub fn new(value: isize) -> Self {
            Simple { value: value }
        }
        pub fn update(&self, f: Box<Fn(&Simple) -> Simple>) -> Self {
            f(self)
        }
        pub fn set(&self, value: isize) -> Self {
            Self::new(value)
        }
    }

    fn simple_reducer(action: SimpleAction, state: &Simple) -> Simple {
        match action {
            SimpleAction::Update(f) => state.update(f),
        }
    }


    #[test]
    fn test_closures() {
        let store = StoreBuilder::new()
            .reducer::<SimpleAction>(Simple::new(0), simple_reducer)
            .build();

        let value = 1;
        store.dispatch(SimpleAction::Update(
            Box::new(move |simple: &Simple| simple.set(value)),
        ));
        assert_eq!(
            store.get_state().get::<Simple>().unwrap(),
            &Simple::new(value)
        );
    }
}
