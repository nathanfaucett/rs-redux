use std::any::{Any, TypeId};
use std::collections::HashMap;

use super::action::Action;
use super::state::State;
use super::store::Store;


pub struct StoreBuilder {
    pub state: State,
    pub functions: HashMap<TypeId, Box<Any>>,
    pub middlewares: HashMap<TypeId, Vec<Box<Any>>>,
}

impl StoreBuilder {
    #[inline(always)]
    pub fn new() -> Self {
        StoreBuilder {
            state: State::new(),
            functions: HashMap::new(),
            middlewares: HashMap::new(),
        }
    }

    #[inline]
    pub fn reducer<A>(mut self, state: A::State, f: fn(A, &A::State) -> A::State) -> Self
    where
        A: Action,
    {
        self.state.set_mut(state);
        self.functions.insert(TypeId::of::<A::State>(), Box::new(f));
        self
    }

    #[inline]
    pub fn middleware<A>(mut self, m: fn(&A, &Store)) -> Self
    where
        A: Action,
    {
        let type_id = TypeId::of::<A::State>();

        if !self.middlewares.contains_key(&type_id) {
            self.middlewares.insert(type_id.clone(), Vec::new());
        }
        {
            let mut middlewares = self.middlewares.get_mut(&type_id).unwrap();
            middlewares.push(Box::new(m));
        }
        self
    }

    #[inline]
    pub fn build(self) -> Store {
        Store::from(self)
    }
}
